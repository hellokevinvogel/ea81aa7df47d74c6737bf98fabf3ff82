import { StepBase } from '../common/base';
import { Order, MenuItem } from '../common/entity';

export class Step1 extends StepBase {
  constructor(lines: string[]) {
    super(lines);

    this.init();
  }

  public init(): void {
    const n: number = Number.parseInt(this.lines[1]) + 2;

    this.setMenuItems(n);

    for (let i = n; i < this.lines.length; i++) {
      this.setOrder(i);
    }
  }

  private setOrder(i: number): void {
    const raw: string[] = this.lines[i].split(' ');
    const order: Order = new Order();

    order.tableId = Number.parseInt(raw[1], 10);
    order.menuItemId = Number.parseInt(raw[2], 10);
    order.quantity = Number.parseInt(raw[3], 10) || 1;

    const menuItem: MenuItem = this.menu.items.find((item: MenuItem) => item.id === order.menuItemId);

    if (!menuItem) {
      return;
    }

    if (menuItem.stock >= order.quantity) {
      for (let i = 0; i < order.quantity; i++) {
        menuItem.decreaseStock();

        console.log(`received order ${order.tableId} ${order.menuItemId}`);
      }
    } else {
      console.log(`sold out ${order.tableId}`);
    }
  }
}
