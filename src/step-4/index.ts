import { StepBase } from '../common/base';
import { CmdType } from '../common/enum';
import { MenuItem, Order, OrderState } from '../common/entity';

export class Step4 extends StepBase {
  public orders: Order[];

  constructor(lines: string[]) {
    super(lines);

    this.orders = [];

    this.init();
  }

  public init(): void {
    const n: number = Number.parseInt(this.lines[1]) + 2;

    this.setMenuItems(n);

    for (let i = n; i < this.lines.length; i++) {
      const raw: string[] = this.lines[i].split(' ');
      const type: CmdType = raw[0] as CmdType;

      if (type === CmdType.ReceivedOrder) {
        this.receive(raw);
      } else if (type === CmdType.Ready) {
        this.ready(raw);
      } else if (type === CmdType.Check) {
        this.complete(raw);
      }
    }
  }

  public receive(raw: string[]): void {
    const order: Order = new Order();

    order.tableId = Number.parseInt(raw[2]);
    order.menuItemId = Number.parseInt(raw[3]);

    this.orders.push(order);
  }

  public ready(raw: string[]): void {
    const tableId: number = Number.parseInt(raw[1]);
    const order: Order = this.orders.find((order: Order) => order.tableId === tableId);

    order.state = OrderState.Ready;
  }

  /*
   * Bug: This should log "0" sometimes. Maybe it's fixed now. Can't test it anymore.
   */
  public complete(raw: string[]): void {
    const tableId: number = Number.parseInt(raw[1]);
    const order: Order = this.orders.find((order: Order) => order.tableId === tableId && order.state === OrderState.Ready);

    if (order) {
      const menuItem: MenuItem = this.menu.items.find((item: MenuItem) => item.id === order.menuItemId);

      if (menuItem) {
        order.state = OrderState.Completed;

        console.log(`${menuItem.price}`);
      } else {
        console.log(0);
      }
    } else {
      console.log('please wait');
    }
  }
}
