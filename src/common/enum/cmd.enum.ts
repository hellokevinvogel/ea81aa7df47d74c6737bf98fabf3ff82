export enum CmdType {
  ReceivedOrder = 'received',
  Complete = 'complete',
  Ready = 'ready',
  Check = 'check',
}
