import { MenuItem } from './menu-item';

export class Menu {
  public items: MenuItem[];

  constructor() {
    this.items = [];
  }

  public addItem(item: MenuItem): void | undefined {
    if (!item) {
      return;
    }

    this.items.push(item);
  }
}
