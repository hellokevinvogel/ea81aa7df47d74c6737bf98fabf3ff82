export class MenuItem {
  public id: number;
  public stock: number;
  public price: number;

  constructor(raw: string[]) {
    if (!raw || raw.length < 3) {
      return;
    }

    this.id = Number.parseInt(raw[0], 10);
    this.stock = Number.parseInt(raw[1], 10);
    this.price = Number.parseInt(raw[2], 10);
  }

  public decreaseStock(size: number = 1): void {
    if (!this.stock) {
      return;
    }

    this.stock = this.stock - size;
  }

  public increaseStock(size: number = 1): void {
    this.stock = this.stock + size;
  }
}
