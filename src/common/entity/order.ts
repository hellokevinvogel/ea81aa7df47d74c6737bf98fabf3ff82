export enum OrderState {
  Open = 0,
  Ready = 1,
  Completed = 2,
}

export class Order {
  public id!: number;
  public state!: OrderState;
  public tableId: number;
  public menuItemId: number;
  public quantity?: number;

  constructor() {
    this.id = Date.now();
    this.state = OrderState.Open;
  }
}
