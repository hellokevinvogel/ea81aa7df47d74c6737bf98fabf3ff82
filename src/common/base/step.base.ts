import { Menu } from '../entity/menu';
import { MenuItem } from '../entity/menu-item';

export class StepBase {
  public menu: Menu;
  public lines: string[];

  constructor(lines: string[]) {
    this.lines = lines;
    this.menu = new Menu();
  }

  protected setMenuItems(n: number, start: number = 2): void {
    for (let i = start; i < n; i++) {
      const raw: string[] = this.lines[i].split(' ');
      const item: MenuItem = new MenuItem(raw);

      this.menu.addItem(item);
    }
  }
}
