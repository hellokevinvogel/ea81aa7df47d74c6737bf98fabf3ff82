import { StepType } from './common/enum/step.enum';
import { Step1 } from './step-1';
import { Step2 } from './step-2';
import { Step3 } from './step-3';
import { Step4 } from './step-4';

function main(lines: string[]): void {
  console.log('Inputs:');

  lines.forEach((v, i) => console.log(`lines[${i}]: ${v}`));

  console.log('');
  console.log('Outputs:');

  if (lines[0] === StepType.Order) {
    new Step1(lines);
  } else if (lines[0] === '2') {
    new Step2(lines);
  } else if (lines[0] === '3') {
    new Step3(lines);
  } else if (lines[0] === '4') {
    new Step4(lines);
  }

  console.log('');
}

function runWithStdin() {
  let input: string = '';

  process.stdin.resume();
  process.stdin.setEncoding('utf8');

  process.stdin.on('data', (v) => (input += v));
  process.stdin.on('end', () => main(input.split('\n')));
}

runWithStdin();
