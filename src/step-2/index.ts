import { StepBase } from '../common/base';
import { CmdType } from '../common/enum';

export class Step2 extends StepBase {
  public microwavesCount: number;
  public microwaves: number[];
  public waitlist: number[];

  constructor(lines: string[]) {
    super(lines);

    this.microwavesCount = 0;
    this.microwaves = [];
    this.waitlist = [];

    this.init();
  }

  public init(): void {
    const n: number = Number.parseInt(this.lines[1]) + 2;
    const [_, microwavesCount]: string[] = this.lines[1].split(' ');

    this.microwavesCount = Number.parseInt(microwavesCount, 10);
    this.setMenuItems(n);

    for (let i = n; i < this.lines.length; i++) {
      const raw: string[] = this.lines[i].split(' ');
      const type: CmdType = raw[0] as CmdType;

      if (type === CmdType.ReceivedOrder) {
        this.receivedOrder(raw);
      } else if (type === CmdType.Complete) {
        this.completeOrder(raw);
      }
    }
  }

  private receivedOrder(raw: string[]): void {
    const menuItemId: number = Number.parseInt(raw[3]);

    if (this.microwavesCount > 0) {
      this.microwaves.push(menuItemId);
      this.microwavesCount--;

      console.log(menuItemId);
    } else {
      this.waitlist.push(menuItemId);

      console.log('wait');
    }
  }

  private completeOrder(raw: string[]): void {
    const menuItemId: number = Number.parseInt(raw[1]);
    const index: number = this.microwaves.indexOf(menuItemId);

    if (index > -1) {
      this.microwaves.splice(index, 1);
      this.microwavesCount++;

      if (this.waitlist.length) {
        const menuItemId: number = this.waitlist.shift();

        this.microwavesCount--;
        this.microwaves.push(menuItemId);

        console.log(`ok ${menuItemId}`);
      } else {
        console.log('ok');
      }
    } else {
      console.log('unexpected input');
    }
  }
}
