import { StepBase } from '../common/base';
import { CmdType } from '../common/enum';
import { Order } from '../common/entity';

export class Step3 extends StepBase {
  public orders: Order[];

  constructor(lines: string[]) {
    super(lines);

    this.orders = [];

    this.init();
  }

  public init(): void {
    const n: number = Number.parseInt(this.lines[1]) + 2;

    this.setMenuItems(n);

    for (let i = n; i < this.lines.length; i++) {
      const raw: string[] = this.lines[i].split(' ');
      const type: CmdType = raw[0] as CmdType;

      if (type === CmdType.ReceivedOrder) {
        this.addOrder(raw);
      } else if (type === CmdType.Complete) {
        this.setReady(raw);
      }
    }
  }

  public addOrder(raw: string[]): void {
    const order: Order = new Order();

    order.tableId = Number.parseInt(raw[2], 10);
    order.menuItemId = Number.parseInt(raw[3], 10);

    this.orders.push(order);
  }

  public setReady(raw: string[]): void {
    const menuItemId: number = Number.parseInt(raw[1], 10);
    const index: number = this.orders.findIndex((order: Order) => order.menuItemId === menuItemId);
    const order: Order = this.orders[index];

    this.orders.splice(index, 1);

    console.log(`ready ${order.tableId} ${order.menuItemId}`);
  }
}
