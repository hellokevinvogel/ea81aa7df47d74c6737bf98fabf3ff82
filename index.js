'use strict';

// My original solution

const menu = {};
// const orders = {};
const MICROWAVES = [];
const WAITLIST = [];
let MICROWAVES_COUNT = 0;

function main(lines) {
  // lines: Array<string>
  /**
   *
   * This is a sample code to use stdin and stdout.
   * You can edit and even remove this code as you like.
   */
  if (lines[0] === '1') {
    step1(lines);
  } else if (lines[0] === '2') {
    step2(lines);
  } else if (lines[0] === '3') {
    step3(lines);
  } else if (lines[0] === '4') {
    step4(lines);
  }

  // console.log('main');

  lines.forEach((v, i) => {
    // console.log(`lines[${i}]: ${v}`);
  });
}

function runWithStdin() {
  let input = '';
  process.stdin.resume();
  process.stdin.setEncoding('utf8');

  process.stdin.on('data', (v) => {
    input += v;
  });
  process.stdin.on('end', () => {
    main(input.split('\n'));
  });
}

runWithStdin();

// step 3

function step4(lines) {
  const n = Number.parseInt(lines[1]) + 2;
  const orders = {};
  const ready = {};

  for (let i = 2; i < n; i++) {
    const menuItemData = lines[i].split(' ');

    menu[menuItemData[0]] = { stock: Number.parseInt(menuItemData[1]), price: Number.parseInt(menuItemData[2]) };
  }

  for (let i = n; i < lines.length; i++) {
    const lineData = lines[i].split(' ');

    if (lineData[0] === 'received') {
      orders[lineData[2]] = { menuId: lineData[3], isReady: false };
    } else if (lineData[0] === 'ready') {
      const tableId = lineData[1];

      if (orders[tableId]) {
        ready[tableId] = orders[tableId];
        delete orders[tableId];
      }
    } else if (lineData[0] === 'check') {
      const tableId = lineData[1];

      if (ready[tableId] && menu[ready[tableId].menuId]) {
        console.log(`${menu[ready[tableId].menuId].price}`);
        delete ready[tableId];
      } else {
        console.log('please wait'); // should be 0 smtms
      }
    }
  }

  // console.log({ menu });
  // console.log({ orders });
}

// step 3

function step3(lines) {
  const n = Number.parseInt(lines[1]) + 2;
  const o = [];

  for (let i = 2; i < n; i++) {
    const menuItemData = lines[i].split(' ');

    menu[menuItemData[0]] = { stock: Number.parseInt(menuItemData[1]), price: Number.parseInt(menuItemData[2]) };
  }

  for (let i = n; i < lines.length; i++) {
    const lineData = lines[i].split(' ');

    if (lineData[0] === 'received') {
      o.push({ tableId: lineData[2], menuId: lineData[3] });
    } else if (lineData[0] === 'complete') {
      const index = o.findIndex((itemData) => itemData.menuId === lineData[1]);
      const item = o[index];
      o.splice(index, 1);
      console.log(`ready ${item.tableId} ${item.menuId}`);
    }
  }
}

// step 2

function step2(lines) {
  const data = lines[1].split(' ');
  MICROWAVES_COUNT = data[1];

  const n = Number.parseInt(lines[1]) + 2;

  for (let i = 2; i < n; i++) {
    const menuItemData = lines[i].split(' ');

    menu[menuItemData[0]] = { stock: Number.parseInt(menuItemData[1]), price: Number.parseInt(menuItemData[2]) };
  }

  for (let i = n; i < lines.length; i++) {
    const lineData = lines[i].split(' ');

    if (lineData[0] === 'received') {
      receivedOrder(lineData);
    } else if (lineData[0] === 'complete') {
      complete(lineData);
    }
  }
}

function receivedOrder(data) {
  // console.log('receivedOrder', { data });
  if (MICROWAVES_COUNT > 0) {
    MICROWAVES.push(data[3]);
    MICROWAVES_COUNT--;
    console.log(data[3]);
  } else {
    WAITLIST.push(data[3]);
    console.log('wait');
  }
}

function complete(data) {
  const menuItemId = data[1];
  const index = MICROWAVES.indexOf(menuItemId);
  // console.log(index, 'complete', { data, MICROWAVES });

  if (index > -1) {
    MICROWAVES_COUNT++;
    MICROWAVES.splice(index, 1);

    if (WAITLIST.length) {
      const menuItemId = WAITLIST.shift();
      MICROWAVES_COUNT--;
      MICROWAVES.push(menuItemId);
      console.log(`ok ${menuItemId}`);
    } else {
      console.log('ok');
    }
  } else {
    console.log('unexpected input');
  }
}

// step 1

function step1(lines) {
  const n = Number.parseInt(lines[1]) + 2;

  for (let i = 2; i < n; i++) {
    const menuItemData = lines[i].split(' ');

    menu[menuItemData[0]] = { stock: Number.parseInt(menuItemData[1]), price: Number.parseInt(menuItemData[2]) };
  }

  for (let i = n; i < lines.length; i++) {
    const order = {};
    const split = lines[i].split(' ');

    order.tableId = split[1];
    order.menuId = split[2];
    order.quantity = split[3];

    const menuItem = menu[order.menuId];

    if (menuItem) {
      if (menuItem.stock >= order.quantity) {
        for (let i = 0; i < order.quantity; i++) {
          console.log(`received order ${order.tableId} ${order.menuId}`);
          menuItem.stock = menuItem.stock - 1;
        }
      } else {
        console.log(`sold out ${order.tableId}`);
      }
    }
  }
}
